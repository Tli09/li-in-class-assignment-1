<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>battlefiled-process</title>
    
</head>
<body>
<?php

require 'database.php';

if(isset($_POST['ammo'])&&isset($_POST['soldiers'])&&isset($_POST['duration'])&&isset($_POST['critique']))
{
    
    $ammo = (int)$_POST['ammo'];
    $soldiers =(int) $_POST['soldiers'];
    $duration = (int)$_POST['duration'];
    $critique = $_POST['critique'];
    
    $stmt = $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique) values (?, ?, ?,?)");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
 
    $stmt->bind_param('iiis', $ammo, $soldiers, $duration, $critique);
 
    $stmt->execute();
 
    $stmt->close();
 
    header("Location: battlefield-submit.html");
            exit;
}
    else{
        echo "ERROR: No Input info!";
        exit;
    
}
    ?>
</body>
</html>