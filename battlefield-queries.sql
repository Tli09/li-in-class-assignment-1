{\rtf1\ansi\ansicpg1252\cocoartf1187\cocoasubrtf340
{\fonttbl\f0\fmodern\fcharset0 Courier-Bold;\f1\fmodern\fcharset0 Courier;}
{\colortbl;\red255\green255\blue255;\red133\green35\blue39;\red247\green247\blue247;\red87\green197\blue83;
\red153\green137\blue39;\red152\green26\blue135;\red247\green247\blue247;}
\margl1440\margr1440\vieww15480\viewh8340\viewkind0
\deftab720
\pard\pardeftab720

\f0\b\fs32 \cf2 \cb3 CREATE TABLE
\f1\b0 \cf0  reports\cf4 (\cf0 \
	id 
\f0\b \cf5 MEDIUMINT
\f1\b0 \cf0  
\f0\b \cf6 UNSIGNED
\f1\b0 \cf0  
\f0\b \cf6 NOT NULL
\f1\b0 \cf0  
\f0\b \cf6 AUTO_INCREMENT
\f1\b0 \cf0 ,\
	ammunition smallint unsigned not null,\
\pard\pardeftab720
\cf0 \cb7     soldiers smallint unsigned not null,\
	duration decimal(6,1) unsigned not null,\cb3 \
\pard\pardeftab720
\cf0 	critique tinytext,\
	posted 
\f0\b \cf2 TIMESTAMP
\f1\b0 \cf0  
\f0\b \cf6 NOT NULL
\f1\b0 \cf0  
\f0\b \cf6 DEFAULT
\f1\b0 \cf0  
\f0\b \cf2 CURRENT_TIMESTAMP,\
  PRIMARY KEY
\f1\b0 \cf0  \cf4 (\cf0 id\cf4 )\cf0 \
\pard\pardeftab720
\cf4 )\cf0  engine \cf4 =\cf0  
\f0\b \cf2 INNODB
\f1\b0 \cf0  
\f0\b \cf6 DEFAULT
\f1\b0 \cf0  character 
\f0\b \cf2 SET
\f1\b0 \cf0  \cf4 =\cf0  utf8 
\f0\b \cf2 COLLATE
\f1\b0 \cf0  \cf4 =\cf0  utf8_general_ci;\
}