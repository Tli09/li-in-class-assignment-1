<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h2{
	text-align: center;
}
</style>
</head>
<body><div id="main">
 
<h2>Latest Critiques</h2>
<p>The info about the battle filed:</p>
<?php
require 'database.php';
 
$stmt = $mysqli->prepare("select count(*),id, critique from employees group by id");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

 
$stmt->execute();
 
$stmt->bind_result(cnt,$id, $critique);
 
echo "<ul>\n";
if(cnt<=5){
	while($stmt->fetch()){
	printf("\t<li>the %i critique is %s</li>\n",
		htmlspecialchars($id),
		htmlspecialchars($critique)
	);
}
}

else{
	$i = 1;
	while($i<5){
		$id=$cnt - i;
	$stmt = $mysqli->prepare("select id, critique from employees where id=?");
	if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
	}

	$stmt->bind_param('i', $id);
 
	$stmt->execute();
 
	$stmt->bind_result($id, $critique);
	printf("\t<li>the %i critique is %s</li>\n",
		htmlspecialchars($id),
		htmlspecialchars($critique)
	);
}
echo "</ul>\n";

$stmt->close();
$i = $i+1;
 
}
?>
</div></body>
</html>